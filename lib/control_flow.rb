# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.reject { |el| el == el.downcase }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str.length.odd? ? str[str.length / 2] : str[(str.length/2)-1, 2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count { |el| VOWELS.include?(el) }
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  (1..num).each { |el| product *= el }
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each do |el|
    joined << el << separator
  end
  separator == "" ? joined : joined[0...-1]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_word = ""
  str.chars.each_with_index do |ch, idx|
    idx.odd? ? new_word << ch.upcase : new_word << ch.downcase
  end
  new_word
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map do |el|
    el.length >= 5 ? el.reverse : el
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each do |num|
    if num % 15 == 0
      result << "fizzbuzz"
    elsif num % 3 == 0
      result << "fizz"
    elsif num % 5 == 0
      result << "buzz"
    else
      result << num
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  arr.each { |el| reversed.unshift(el) }
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  num == 1 ? false : (2...num).none? { |el| num % el == 0 }
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |el| num % el == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).select { |el| num % el == 0 if prime?(el) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_count = arr.count(&:even?)
  odd_count = arr.length - even_count
  even_count > odd_count ? arr.select(&:odd?)[0] : arr.select(&:even?)[0]
end
